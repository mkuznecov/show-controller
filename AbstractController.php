<?php
require $_SERVER[ 'DOCUMENT_ROOT' ] . '/bitrix/modules/main/include/prolog_before.php';
CModule::IncludeModule( 'iblock' );

interface iIndex {
    public static function recordFile ( $file );
    public static function getListFile ();
    public function getFullFile ();
    public function getAllFile ();
    public function testSkill ();
    public static function delFile( $id );
    public static function disableFile( $id );
    public static function setPassZip ( $fileName, $pass, $sleep );
    public static function generationKey ( );
    public static function folderSize ( $dir );
    public static function delOneFile ( );
    public static function getConfig ( );
}

abstract class AbstractController implements iIndex {
    
    /*
     * $file array данне из массива $_FILES
     * return array данные о записанном файле 
     */
    public static function recordFile ( $file ) {
        $tmpFile = [
            'name'      => $file['name'],
            'size'      => $file['size'],
            'tmp_name'  => $file['tmp_name'],
            'type'      => '',
            'old_file'  => '',
            'del'       => 'Y',
            'MODULE_ID' => 'iblock'
        ];

        $tmp = CFile::SaveFile($tmpFile, 'hl123');

        $request = [
            'FILE' => $tmp,
            'NAME' => $file['name'],
        ];

        return $request;
    }
    /*
     * return array файлы для отображения в шаблоне
     */
    public static function getListFile () {
        $request = [];

        $isHttps = !empty($_SERVER['HTTPS']) && 'off' !== strtolower($_SERVER['HTTPS']);
        global $USER;
        $res = CIBlockElement::GetList( 
            ["SORT"=>"ASC"], 
            [
                'IBLOCK_ID'     => 1,
                'PROPERTY_USER' => ( array_search( '8', $USER->GetParam('GROUPS') ) === false )? $USER->GetID( ) : '' ,
                'ACTIVE'        => ( array_search( '8', $USER->GetParam('GROUPS') ) === false )? 'Y' : ''
            ],
            false,
            false,
            [
                'ID', 
                'IBLOCK_ID', 
                'NAME', 
                'ACTIVE',
                'DATE_ACTIVE_FROM',
                'PROPERTY_USER',
                'PROPERTY_FILE',
                'PROPERTY_CREATION_TIME',
                'PROPERTY_DATE_KILL',
                'PROPERTY_LINK',
                'PROPERTY_KEY',
            ]
        );

        while($ob = $res->Fetch()){ 

            if ( $ob['PROPERTY_FILE_VALUE'] != null ){ 

                $rsUser = CUser::GetByID( $ob['PROPERTY_USER_VALUE'] );
                $arUser = $rsUser->Fetch();
                $request[ ] = [
                    'NAME'          => CFile::GetByID( $ob[ 'PROPERTY_FILE_VALUE' ] )->arResult[0][ 'FILE_NAME' ],
                    'SRC2'          => CFile::GetPath( $ob[ 'PROPERTY_FILE_VALUE' ] ),
                    'HASH'          => $ob[ 'PROPERTY_LINK_VALUE' ],
                    'DATE_CREATE'   => $ob[ 'PROPERTY_CREATION_TIME_VALUE' ],
                    'DATE_KILL'     => $ob[ 'PROPERTY_DATE_KILL_VALUE' ],
                    'FILE_ID'       => $ob[ 'PROPERTY_FILE_VALUE' ] ,
                    'ID'            => $ob[ 'ID' ],
                    'EMAIL_CREATER' => $arUser[ 'EMAIL' ],
                    'ACTIVE'        => $ob[ 'ACTIVE' ],
                    'KEY'           => $ob[ 'PROPERTY_KEY_VALUE' ],
                    'FILE_SIZE'     => CFile::GetByID( $ob[ 'PROPERTY_FILE_VALUE' ] )->arResult[0]['FILE_SIZE'],
                ];

            }

        }

        return array_reverse( $request );
    }
    /*
     * return array файлы для отображения менеджеру
     */
    public function getAllFile () {
        $request = [];

        $res = CIBlockElement::GetList( 
            [], 
            [
                'IBLOCK_ID'     => 1,
                'PROPERTY_USER' => $USER->GetID( ),
            ],
            false,
            false,
            [
                'ID', 
                'PROPERTY_USER',
                'PROPERTY_FILE',
                'PROPERTY_CREATION_TIME',
                'PROPERTY_DATE_KILL',
             ]
        );

        while($ob = $res->Fetch()){ 
            if ( $ob['PROPERTY_FILE_VALUE'] != null ){ 
                $request[] = $ob;
            }
        }

        return $request;
    }
    /*
     * Проверяет есть ли квалификация у пользователя
     */
    public static function testSkill () {
        global $USER;
        $rsUser = CUser::GetByID( $USER->GetID() );
        $arUser = $rsUser->Fetch();

        if ( $arUser['UF_SKILL'] != 1 ) die('Для продолжения работы необходима квалификация');
    }
    /*
     * Удаляет файл полностью из системы 
     * $id int идентификатор в инфоблоке 
     */
    public static function delFile( $id ) {
        if ( $id > 0 ) {
            global $USER;
            $res = CIBlockElement::GetList( 
                [], 
                [
                    'IBLOCK_ID' => 1,
                    'ID'        => $id,
                ],
                false,
                false,
                [
                    'ID', 
                    'PROPERTY_FILE',
                    'PROPERTY_USER'
                 ]
            );

            while($ob = $res->Fetch()){ 
                
                if ( array_search( '8', $USER->GetParam('GROUPS') ) !== false ) {
                    CFile::Delete( $ob["PROPERTY_FILE_VALUE"] );
                    CIBlockElement::Delete( $id );
                }
            }

        }

        return null;
    }

    /*
     * $id int Идентификатор файла
     * return int Идентификатор записи которая была заблокирована
     */
    public static function disableFile( $id ){
        if ( $id > 0 ) {
            global $USER;
            $res = CIBlockElement::GetList( 
                [], 
                [
                    'IBLOCK_ID' => 1,
                    'ID'        => $id,
                ],
                false,
                false,
                [
                    'ID', 
                    'PROPERTY_FILE',
                    'PROPERTY_USER'
                 ]
            );

            while($ob = $res->Fetch()){ 
                
                if ( $ob[ 'PROPERTY_USER_VALUE' ] == $USER->GetID() ) {
                    $el = new CIBlockElement; 
                    return $el->Update( $id, [ "ACTIVE" => "N" ] );
                }
            }

        }
    }
    /*
     * Устанавливает на архив пароль. (Архив должен быть без пароля)
     * $fileName string путь до архива
     * $pass string пароль для архива
     * $sleep string Задержка перед отправкой 
     * return bool результат выполнения
     */
    public static function setPassZip ( $fileName, $pass, $sleep ) {
        ob_end_flush();
        $bashdir = $_SERVER[ 'DOCUMENT_ROOT' ] . "/api/Controller/set.pass";
        $bash = str_replace( '[filename]', $fileName, ( str_replace('[password]', $pass, file_get_contents($bashdir) ) ) ) ;
        
        return exec($bash);
    }
    /*
     * return string пароль 
     */
    public static function generationKey() {
        $len = 8;
        $dictionary = [
            'a','b','c','d','e','f',
            'g','h','i','j','k','l',
            'm','n','o','p','r','s',
            't','u','v','x','y','z',
            'A','B','C','D','E','F',
            'G','H','I','J','K','L',
            'M','N','O','P','R','S',
            'T','U','V','X','Y','Z',
            '1','2','3','4','5','6',
            '7','8','9','0','.',',',
            '(',')','[',']','!','?',
            '&','^','%','@','*','$',
            '<','>','/','|','+','-',
            '{','}','`','~'
        ]; // Словарь 
        $pass = '';
        $tmpArr = [];
        $lim = [ 
            24,
            48,
            58,
            77
        ];
        for ( $i = 0; $i < $len; $i++ ) $tmpArr[] = $i;
        $rand_keys = array_rand( $tmpArr, $len/2 );
        for( $i = 0; $i < $len; $i++ ) {
            $selectedSymbol = array_search($i, $rand_keys);
            if ( $selectedSymbol !== false ) {
                $pass .= $dictionary[ rand( ( $selectedSymbol == 0 )? 0 : $lim[$selectedSymbol-1] + 1 ,$lim[$selectedSymbol]) ];
            } else {
                $pass .= $dictionary[ rand(0, count( $dictionary ) - 1) ];
            }
        }

        return $pass;
    }
    /*
     * $dir string путь в папке
     * Размер папки в которой находятся файлы 
     */
    public static function folderSize ( $dir ) {
        $size;

        foreach ( glob( rtrim( $dir, '/' ).'/*', GLOB_NOSORT ) as $each ) {
            $size += is_file( $each ) ? filesize( $each ) : self::folderSize( $each );
        }

        return $size;
    }
    /*
     * Удаляет один файл который имеет статус не активный 
     */
    public static function delOneFile( ){
        global $USER;

        $res = CIBlockElement::GetList( 
            [
                'SORT' => 'ASC'
            ], 
            [
                'IBLOCK_ID' => 1,
                'ACTIVE'    => 'N'
            ],
            false,
            [
                'nPageSize' => 1
            ],
            [
                'ID', 
            ]
        );

        while($ob = $res->Fetch()){ 
            self::delFile( $ob['ID'] );
        }

        return null;
    }
    /*
     * Подтягиваем конфигурационные данные.
     * return array конфигурация
     */
    public static function getConfig() {

        $request = [
            'del'          => 24,
            'notification' => 10,
            'size'         => 100000,
        ];

        $res = CIBlockElement::GetList( 
            [],
            [ 
                "IBLOCK_ID" => 5,
                "ACTIVE"    => "Y" 
            ],
            false,
            [ "nPageSize" => 1 ], 
            [ 
                "PROPERTY_TIME_DEL",
                "PROPERTY_NOTIFICATION",
                "PROPERTY_SIZE",
            ]
        );

        while($ob = $res->Fetch() ) {
            $request = [
                'del'          => ( (int)$ob[ 'PROPERTY_TIME_DEL_VALUE' ] >= 1 and (int)$ob[ 'PROPERTY_TIME_DEL_VALUE' ] <= 1000 )? (int)$ob[ 'PROPERTY_TIME_DEL_VALUE' ] : 24,
                'notification' => ( (int)$ob[ 'PROPERTY_NOTIFICATION_VALUE' ] >= 1 and (int)$ob[ 'PROPERTY_NOTIFICATION_VALUE' ] <= 1000 )? (int)$ob[ 'PROPERTY_NOTIFICATION_VALUE' ] : 10,
                'size'         => ( (int)$ob[ 'PROPERTY_SIZE_VALUE' ] >= 1000 and (int)$ob[ 'PROPERTY_SIZE_VALUE' ] <= 100000000 )? (int)$ob[ 'PROPERTY_NOTIFICATION_VALUE' ] : 100000,
            ];
        }

        return $request;
    }

}

?>